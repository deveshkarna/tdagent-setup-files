import json
import requests
import subprocess
import sys
import traceback


EXIT_OK = 0
EXIT_WARNING = 1
EXIT_ERROR = 2
EXIT_UNKNOWN = 3
HOST = '127.0.0.1'
PORT = '24220'
URL = 'http://' + HOST + ':' + PORT + '/api/plugins.json'
MONIT_CONFIG_PATH = '/etc/knowlus/monit_services'
FILE_LIST = {
    'outcalld': ['log'],
    'incalld': ['log'],
    'postprocessd': ['log','cdr'],
    'twisted_reactord': ['log'],
    'forwarderd': ['log'],
    'krm_message_managerd': ['log'],
    'krpycd': ['log'],
    'krpycd_incalld': ['log'],
    'new_preprocessd': ['log'],
    'reactord': ['log']
}
versions = list()
input_plugin_path = list()
running_services = list() # stores list of running services in given machine from monit config
expected_input_plugin_path = list()

class WarningException(Exception):
    pass

class ErrorException(Exception):
    pass

class UnknownException(Exception):
    pass


def get_expected_plugin_path():
    cdr_set = False
    for service in running_services:
        if FILE_LIST.has_key(service[:-4]):
            file_type = FILE_LIST.get(service[:-4])
            for type in file_type:
                if type == "cdr":
                     if not cdr_set:
                        cdr_set = not cdr_set
                        expected_input_plugin_path.append("/var/log/knowlus/%s*.%s" % (service[:-4], type))
                elif service.endswith("_new"):
                    expected_input_plugin_path.append("/var/log/knowlus/%s_%s.%s" % (service[:-4], versions[1], type))
                else:
                    expected_input_plugin_path.append("/var/log/knowlus/%s_%s.%s" % (service[:-4], versions[0], type))
        else:
            print ("Unknown service %s", service)
            return False
    expected_input_plugin_path.sort()
    return True

def compare_data():
    if get_expected_plugin_path():
        if len(input_plugin_path) != len(expected_input_plugin_path):
            print ("length of expected and actual plugin path is different")
            return False
        for i in range(len(input_plugin_path)):
            if expected_input_plugin_path[i] != input_plugin_path[i]:
                return False
        return True
    else:
        return False

def get_services_list():
    monit_filehandle = open(MONIT_CONFIG_PATH, 'r')
    lines = monit_filehandle.readlines()
    for line in lines:
        line = line.strip()
        if len(line)>3 and not line.startswith('#'):
            running_services.append(line)
            if line.startswith("reactord"):
                running_services.append("twisted_"+ line)
    monit_filehandle.close()

def get_knowlus_version():
    ps_output = subprocess.Popen("ps aux | grep 'monit'|grep -v grep | awk '{print $20}'", shell=True, stdout=subprocess.PIPE)
    monit_version = ps_output.communicate()[0]
    monit_version = monit_version.split('/')[2].split('-')[1]
    settings_path = "/srv/newknowlus-%s/%s/knowlus/monit/settings.py" % (monit_version, monit_version)
    setting_handle = open(settings_path, 'r')
    lines = setting_handle.readlines()
    count = 0
    for line in lines:
        if line.startswith("OLD") or line.startswith("NEW"):
            line = line.split("=")
            versions.append(line[1].strip()[1:-1])
            count += 1
        if count == 2:
            break
    setting_handle.close()
    versions.sort()


try:
    get_services_list()
    get_knowlus_version()
    try:
        resp = requests.get(URL)
    except:
        print ("Request timed out")
        raise ErrorException
    if resp.status_code == 200:
        resp = json.loads(resp.content)
        plugin_list = resp.get(resp.keys()[0])
        for plugin in plugin_list:
            if plugin.get('plugin_category') == 'input' and plugin.get("type") == "tail":
                input_plugin_path.append(plugin.get('config').get('path'))
        input_plugin_path.sort()
        if compare_data():
            print ("td-agent is running properly")
            raise sys.exit(EXIT_OK)
        else:
            print ("td-agent is not monitoring all files")
            raise WarningException
    else:
        print ("Couldn't get proper response from td-agent")
        raise UnknownException
except SystemExit:
    sys.exit(EXIT_OK)
except WarningException:
    sys.exit(EXIT_WARNING)
except UnknownException:
    sys.exit(EXIT_UNKNOWN)
except ErrorException:
    sys.exit(EXIT_ERROR)
except:
    print (traceback.format_exc())
    print ("Could receive any response from td-agent")
    sys.exit(EXIT_ERROR)


#Sample data
#{"plugins":[{"plugin_id":"object:3fcb0fc9b5ac","plugin_category":"input","type":"monitor_agent","config":{"@type":"monitor_agent","bind":"0.0.0.0","port":"24220","tag":"monitor.metrics"},"output_plugin":false,"retry_count":null},{"plugin_id":"object:3fcb0fc31d8c","plugin_category":"input","type":"tail","config":{"@type":"tail","format":"/^(?<time>\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2},\\d{3}) ([^\\s]+) ([^\\s]+) ([^\\s]+) KBEAT module: (?<module>[^\\s]+) host: (?<host>[^\\s]+) cluster: (?<cluster>[^\\s]+) status: (?<status>[^\\s]+) version: (?<version>[^\\s]+) message: (?<message>.*)/","tag":"outcalld","path":"/var/log/knowlus/outcalld_3.0.0.log","pos_file":"/var/log/td-agent/outcalld_3.0.0.log.pos"},"output_plugin":false,"retry_count":null},{"plugin_id":"object:3fcb0fcb1140","plugin_category":"output","type":"file","config":{"@type":"file","path":"/var/log/fluent/outcalld","format":"json","time_slice_wait":"1m","include_time_key":"true","time_slice_format":"%Y%m%d%H","timezone":"+0530","buffer_path":"/var/log/fluent/outcalld.*"},"output_plugin":true,"buffer_queue_length":0,"buffer_total_queued_size":6000,"retry_count":0}]}
