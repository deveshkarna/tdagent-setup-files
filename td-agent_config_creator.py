import json
import traceback
import sys
import commands

SUCESSFUL_EXIT = 0
UNSUCCESSFUL_EXIT = 1 
AGENT_MONITOR_CONF = "agent_monitor.conf"
LOG_CONFIG_FILE_PATH = "template_log.conf"
CDR_CONFIG_FILE_PATH = "template_cdr.conf"
CONFIG_FILE_DESTINATION = "/etc/td-agent/config.d/"
MONIT_PROCESS_CONFIG_PATH = "/etc/knowlus/monit_services"
KNOWLUS_LOG_PATH = "/var/log/knowlus/"
POS_FILE_PATH = "/var/log/td-agent/"
VERSION_OLD = sys.argv[1]
VERSION_NEW = sys.argv[2]
CLUSTER = sys.argv[3]
RELOAD_COMMAND = "sudo service td-agent reload"
services = list()   # Stores list of services to create conf file
versions = list()   # Stores knowlus versions to create conf file


def validate_release_version(version_list):
    for version in version_list:
        if len(version) < 3:
            print "Please pass valid knowlus version"
            raise Exception("Please pass valid knowlus version")
        version = version.split(".")
        for x in version:
            if x.isdigit():
                continue
            else:
                print "Enter knowlus version in digit"
                raise Exception("Enter knowlus version in digit")

def populate_services_list():
    try:
        cfg_file = open(MONIT_PROCESS_CONFIG_PATH, 'r')
        lines = cfg_file.readlines()
        for line in lines:
            line = line.strip()
            if line.startswith('#') or len(line) < 1:
                continue
            else:
                services.append(line)
        if 'reactord_new' in services:
            services.append('twisted_reactord_new')
        if 'reactord_old' in services:
            services.append('twisted_reactord_old')
        services.sort()
    except IOError:
        print traceback.format_exc()
        raise Exception("Don't have permission to read file")
    except:
        print traceback.format_exc()
        raise Exception("Unknown exception")

"""def get_versions():
    global MONIT_VERSION_CONFIG_PATH
    TODO Need to think for a better approach
    MONIT_VERSION_CONFIG_PATH.replace('3.0.0',release_version)
    try:
        sett_file = open(MONIT_VERSION_CONFIG_PATH, 'r')
        # lines = sett_file.readlines()
        count = 0
        while count<2:
            line = sett_file.readline()
            line = line.strip()
            if line.startswith('OLD_VERSION') or line.startswith('NEW_VERSION'):


        for line.strip() in lines:
            if "OLD_VERSION " in line:
                continue
            else:
                services.append(line)
    except IOError:
        print traceback.format_exc()
        raise Exception("Don't have permission to read file")
    except:
        print traceback.format_exc()
        raise Exception("Unknown exception")"""

def create_conf_dict(conf):
    source_dict = dict()
    match_dict = dict()
    for line in conf:
        if line.startswith('#source'):
            cur_dict = source_dict
        cur_dict[:line.find(" ")] = line[line.find(" ")+1:]
    return source_dict, match_dict


def create_conf_files_log():
    temp_file = open(LOG_CONFIG_FILE_PATH, 'r')
    conf = temp_file.readlines()
    for i, j in enumerate(services):
        service = j
        if service.endswith("new"):
            version = VERSION_NEW
            service = service[:-4]
        else:
            version = VERSION_OLD
            service = service[:-4]
        uniq_tag = service + '_' + str(version)
        conf_file_path = CONFIG_FILE_DESTINATION + service +".conf"
        if i == 0 or services[i-1][:-4] != service:
            conf_file = open(conf_file_path, 'w')
        else:
            conf_file = open(conf_file_path, 'a')
        open_tag = None
        for line in conf:
            line = line[:len(line)-1]
            if line.startswith("#"):
                if open_tag:
                    conf_file.write("</" + open_tag + ">\n\n")  # closing opened tag
                open_tag = line[line.find("#")+1 : len(line)]
                if "match" in open_tag:
                    conf_file.write("<" + open_tag + " " + uniq_tag +">\n")
                else:
                    conf_file.write("<" + open_tag + ">\n")
            else:
                if line.strip().startswith("tag"):
                    conf_file.write(line + " " + uniq_tag + "\n")
                elif line.strip().startswith("path"):
                    conf_file.write(line + uniq_tag + ".log\n")
                elif line.strip().startswith("pos_file"):
                    conf_file.write(line + uniq_tag + ".log.pos\n")
                elif line.strip().startswith("buffer_path"):
                    conf_file.write(line + uniq_tag + ".buffer\n")
                else:
                    conf_file.write(line + "\n")
        conf_file.write("</" + open_tag +">\n\n")
        conf_file.close()

def create_conf_files_cdr():
    if "postprocessd" or "postprocessd_new" in services:
        temp_file = open(CDR_CONFIG_FILE_PATH, 'r')
        conf = temp_file.readlines()
        for service in services:
            if "postprocess" in service:
                if service.endswith("new"):
                    service = service[:-4]
                else:
                    service = service[:-4]
                conf_file_path = CONFIG_FILE_DESTINATION + service + "_cdr" +".conf"
                conf_file = open(conf_file_path, 'w')
                open_tag = ""
                for line in conf:
                    line = line[:len(line)-1]
                    if line.startswith("#"):
                        if open_tag:
                            if "match" in open_tag:
                                open_tag = open_tag.strip().split(" ")[0]
                            conf_file.write("</" + open_tag + ">\n\n")  # closing opened tag
                        open_tag = line[line.find("#")+1 : len(line)]
                        conf_file.write("<" + open_tag + ">\n")
                    else:
                        if line.strip().startswith("cluster"):
                            cluster_name = CLUSTER.strip().split("_")[0].lower()
                            conf_file.write(line + cluster_name + "\n")
                        else:
                            conf_file.write(line + "\n")
                conf_file.write("</match>\n")
                conf_file.close()
                break
            else:
                continue
    else:
        return


def create_self_monitoring_conf():
    filename = CONFIG_FILE_DESTINATION + AGENT_MONITOR_CONF
    write_handle = open(filename,'w')
    read_handle = open(AGENT_MONITOR_CONF,'r')
    conf = read_handle.readlines()
    open_tag = None
    for line in conf:
        line = line[:len(line)-1]
        if line.startswith("#"):
            if open_tag:
                write_handle.write("</" + open_tag + ">\n\n")  # closing opened tag
            open_tag = line[line.find("#")+1 : len(line)]
            write_handle.write("<" + open_tag + ">\n")
        else:
            write_handle.write(line + "\n")
    write_handle.write("</" + open_tag + ">\n\n")
    write_handle.close()
    read_handle.close()

def reload_config():
    commands.getoutput(RELOAD_COMMAND)


try:
    versions.append(VERSION_OLD)
    versions.append(VERSION_NEW)
    validate_release_version(versions)
    populate_services_list()
    create_conf_files_log()
    create_conf_files_cdr()
    create_self_monitoring_conf()
    reload_config()
except:
    print traceback.format_exc()
    print "Script could not execute successfully"
